// Inspired by https://github.com/connachermurphy/typst-cv/
#import "cv_config.typ": *
#show: cv.with(
    name: "John Smith",
    address: "Plaintext Polytechnic, New York, USA",
    email: [#link("mailto:email@example.com")],
    website: "https://johnsmith.typst.net",
    date: [#today.display("[day] [month repr:long] [year]")]
)

= Education
#entry(
  date: [2021-2024],
  description: [Ph.D., Typst University.],
)[]
#entry(
  date: [2019-2021],
  description: [M.A., University of Quarto.],
)[]
#entry(
  date: [2016-2019],
  description: [B.Sc., Latex University.],
)[]

= Employment
#entry(
  date: [2024-Present],
  description: [Assistant Professor, Plaintext Polytechnic.],
)[]

= Publications

== Books
#entry(
  date: [2024],
  description: [#cite(<smith2024>, form: "full").],
)[]

== Peer-reviewed articles
#entry(
  date: [2024],
  description: [#cite(<smith2024>, form: "full").],
)[]

#show bibliography: none
#bibliography(bibfile, style: "chicago-fullnotes")
