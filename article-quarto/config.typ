// Extensions
#import "@preview/wordometer:0.1.1": word-count, total-words
#show: word-count

// Variables
#let title = [Article with Typst in Quarto]
#let author = [John Smith]
#let affiliation = [Typst University]
#let email = [#link("mailto:email@example.com")]
#let abstract = [#lorem(60)]
#let today = datetime.today()
#let date = [Draft dated #today.display("[day] [month repr:long] [year]")]
#let words = [#total-words]       
#let thanks = [A thank you note.]
        
// Settings
#set text(
  font: "Linux Libertine",
  size: 11pt
)
#set page(
  paper: "a4",
  margin: (x: 3cm, y: 3cm),
  numbering: "1",
)
#set par(
  justify: true,
  leading: .6em, // line spacing
)

#show footnote.entry: set text(black)
#set footnote.entry(indent: 0em)

// uncomment for numbered headings
//#set heading(numbering: "1.") 

// Top of first page
#align(center, text(20pt)[
  *#title*  \ \ 
  ])
#align(center, text(13pt)[
  #author #footnote[#thanks] \ 
  #affiliation \ 
  #email \ \ 
  ])
#align(center, text(11pt)[
  #date --- #words words \ \ 
  ])
#align(center)[
  #set par(justify: false)
  *Abstract* \ #abstract
] \

// Rest
#show heading.where(
  level: 1
): it => block(width: 100%)[
  #set align(left)
  #set text(12pt, weight: "bold")
  #smallcaps(it.body)
]

#show heading.where(
  level: 2
): it => block(width: 100%)[
  #set align(left)
  #set text(11pt, weight: "bold")
  #it.body
]

#show: rest => columns(2, rest)