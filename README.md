# typst-docs

Some simple [Typst](https://typst.app/) templates for typical academic documents, notably:

- Article (Typst via Quarto)
- Slides (pure Typst)
- CV (pure Typst)

### Article

<p align="center">
  <img src="article-quarto/manuscript.png" width="70%">
</p>

### Slides

<p align="center">
  <img src="slides/slides-01.png" width="23%">
&nbsp; 
  <img src="slides/slides-02.png" width="23%">
&nbsp; 
  <img src="slides/slides-03.png" width="23%">
&nbsp; 
  <img src="slides/slides-04.png" width="23%">
</p>
<p align="center">
  <img src="slides/slides-05.png" width="23%">
&nbsp; 
  <img src="slides/slides-06.png" width="23%">
&nbsp; 
  <img src="slides/slides-07.png" width="23%">
&nbsp; 
  <img src="slides/slides-08.png" width="23%">
</p>
<p align="center">
  <img src="slides/slides-10.png" width="23%">
&nbsp; 
  <img src="slides/slides-11.png" width="23%">
&nbsp; 
  <img src="slides/slides-13.png" width="23%">
&nbsp; 
  <img src="slides/slides-14.png" width="23%">
</p>

### CV

<p align="center">
  <img src="cv/cv.png" width="70%">
</p>

