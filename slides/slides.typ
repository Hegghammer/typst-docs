// Docs: https://polylux.dev/book/themes/gallery/university.html

#import "@preview/polylux:0.3.1": *
#import themes.university: *
#show: university-theme.with(
  aspect-ratio: "16-9",
  short-title: "Text in middle footer",
  short-author: "Text in left footer",
  short-date: "Text in right footer", 
  color-a: orange.darken(20%), // headers & left footer
  color-b: orange.lighten(30%), // right footer
  color-c: black.lighten(95%), // header background
  progress-bar: true
)

#title-slide(
  title: "Title",
  authors: ("Name or email"),
  subtitle: "Subtitle",
  institution-name: "A date perhaps",
  date: text(12pt)[Maybe a thank you note],
)

#slide(title: [Basic slide], new-section:[First section])[
  #v(3cm)
  - This is a point
  - This is another
]

#slide(title: [Basic split slide])[
  #side-by-side(columns: (2fr, 3fr))[
  #v(3cm)
  - Some text.
  ][
  #v(1cm)
  #align(center)[#image("img.jpg")]
  ]
]

#focus-slide()[Default section slide] 

#focus-slide(background-color: orange)[#align(right+top)[Tweaked section slide]]

#focus-slide(background-img: image("img.jpg"))[Text on background image.\ The next slide is just an image.]

// Just an image
#focus-slide(background-img: image("img.jpg"))[#h(1cm)]

#slide(title: [Text wherever you want])[
  #v(1cm)
  #set align(center)
  Centered as default.
  #align(right)[Temporarily on right.]
  #text(40pt, red)[Ad hoc red.]
  #align(left)[#text(30pt, green)[Green on left.]]
  Back to default.
]

#slide(header: [], footer: [])[
  - This slide has no header
  - Nor does it have a footer
]

#slide(title: [Simple table])[
  
#table(
  columns: (2fr, 2fr, 1fr),
  inset: 10pt,
  align: right+horizon,
  [*Product*], [*Price*], [*Weight*],
  [Bananas], [5], [6 kg],
  [Apples], [8], [1 kg]
)
]

#slide(title: [Tweaked tables])[
  
#table(
  columns: (2fr, 2fr, 1fr),
  align: (x, y) => (left, center, right).at(x),
  inset: 20pt,
  fill: gray.lighten(40%),
  stroke: 4pt + red,
  [*Product*], [*Price*], [*Weight*],
  [Bananas], [5], [6kg],
  [Apples], [8], [1kg]
)

#table(
  columns: (1fr, 1fr, 1fr),
  align: (x, y) => (left, center, right).at(x),
  inset: 10pt,
  fill: yellow.lighten(40%),
  stroke: none,
  [*Product*], [*Price*], [*Weight*],
  [Bananas], [5], [6kg],
  [Apples], [8], [1kg]
)
]

#set page(fill: orange.lighten(30%))
#slide(title: [Different background colour])[
  #v(3cm)
  - This is a point
  - This is another
]

#set page(fill: white)

#slide(title: [Citations])[
  #v(3cm)
  A citation @smith2024.
]

#slide(title: [References])[
  #set text(18pt)
  #bibliography("refs.bib", title: none, style: "chicago-author-date")
]
