// Inspired by https://github.com/connachermurphy/typst-cv/

#let name_size = 20pt
#let heading_font = "Futura"
#let heading1_size = 13pt
#let heading1_weight = "bold"
#let heading2_size = 11pt
#let heading2_weight = "bold"
#let body_font = "Lora"
#let body_size = 11pt
#let dist = -.5em
#let lang = "en"
#let bibfile = "refs.bib"
#let today = datetime.today()
#let cv(
    name: [],
    address: [],
    email: [],
    website: [],
    date: [],
    body,
    ) = {
        set page(paper: "a4", numbering: "1", margin: (left: 2cm, right: 2cm))
        set text(font: body_font, size: body_size, lang: lang)
        show heading.where(level: 1): set text(font: heading_font, size: heading1_size, weight: heading1_weight)
        show heading.where(level: 2): set text(font: heading_font, size: heading2_size, weight: heading2_weight)
        show heading.where(level: 1): set block(above: 1.5em, below: .5em)
        show heading.where(level: 2): set block(above: 1.5em, below: .5em)
        show link: set text(blue.darken(30%))
        show par: set block(above: 0em, below: 0em)
        align(center)[
            #text(
                name, 
                font: heading_font, 
                style: "normal", 
                weight: "regular", 
                size: name_size) \
            #v(.5cm) 
            #address \
            #email \
            #website \
            Last updated #date
            ]
        //line(length: 100%)
        body
    }

#let entry(
    date: [],
    description: [],
    body
    ) = {
        stack(dir: ltr,
        { 
         [#block(width: 2.5cm)[#date #v(dist)]]
        }, 
        { 
         [#block(width: 14.5cm)[#description #v(dist)]]
        }
        )
    }